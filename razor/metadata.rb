# encoding: UTF-8
name             'razor'
maintainer       'Matthieu Bonnard'
maintainer_email 'bonnard.matthieu@gmail.com'
license          'Apache 2.0 License'
description      'Installs/Configures puppetlabs/razor-server'
version          '0.1.0'

%w{ ubuntu fedora }.each do |os|
  supports os
end

depends 'database'
depends 'java'
depends 'nginx'
depends 'postgresql'