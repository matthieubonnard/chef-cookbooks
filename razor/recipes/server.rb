# encoding: UTF-8
#
# Cookbook Name:: razor
# Recipe:: server

# Include dependencies
package node['razor']['libarchive']
package 'unzip'
package 'curl'

include_recipe 'razor::database'

execute 'Add PuppetLabs package repository' do
  command 'rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-fedora-20.noarch.rpm || :'
end

package "razor-server"

directory node['razor']['install']['repo'] do
  owner node['razor']['user']
  group node['razor']['group']
  recursive true
  mode  00755
  action :create
end

directory "#{node['razor']['install']['dest']}/log" do
  owner node['razor']['user']
  group node['razor']['group']
  mode  00755
end

file "#{node['razor']['install']['dest']}/log/production.log" do
  owner node['razor']['user']
  group node['razor']['group']
  mode  00660
end

template "#{node['razor']['install']['dest']}/config.yaml" do
  source 'config.yaml.erb'
  owner node['razor']['user']
  group node['razor']['group']
  mode  00660
end

execute 'Create/Migrate database' do
  command 'razor-admin -e production migrate-database'
  path    ['/usr/local/bin']
  cwd     node['razor']['install']['dest']
  action  :nothing
  subscribes :run, "template[#{node['razor']['install']['dest']}/config.yaml]", :immediately
end

remote_file 'razor-microkernel' do
  source node['razor']['microkernel']['url']
  notifies :run, 'execute[untar razor-microkernel]', :immediately
end

execute 'untar razor-microkernel' do
  command "tar -xvf #{Chef::Config['file_cache_path']}/razor-microkernel.tar -C #{node['razor']['install']['repo']}/"
  creates "#{node['razor']['install']['repo']}/microkernel"
  action :nothing
end

directory node['razor']['install']['repo'] do
  owner node['razor']['user']
  group node['razor']['group']
  recursive true
  mode  00755
  action :create
end