# encoding: UTF-8
#
# Cookbook Name:: razor
# Recipe:: default

include_recipe 'razor::server'
include_recipe 'razor::client'
include_recipe 'razor::webui'
