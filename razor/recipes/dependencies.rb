# encoding: UTF-8
#
# Cookbook Name:: razor
# Recipe:: dependencies

package node['razor']['libarchive']
package 'unzip'
package 'curl'
package 'java-1.8.0-openjdk'  

group node['razor']['group']  
user node['razor']['user'] do
  group node['razor']['group']
    system true
    password '*'
    home node['razor']['torquebox']['dest']
    shell '/bin/bash'
    comment 'razor daemon user'
end

include_recipe 'razor::database'