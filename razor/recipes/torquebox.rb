# encoding: UTF-8
#
# Cookbook Name:: razor
# Recipe:: torquebox
#

remote_file "#{Chef::Config[:file_cache_path]}/razor-torquebox.zip" do
  source    node['razor']['torquebox']['url']
  path      "#{Chef::Config[:file_cache_path]}/razor-torquebox.zip"
  checksum  node['razor']['torquebox']['checksum']
  owner     node['razor']['torquebox']['user']
  group     node['razor']['torquebox']['group']
  mode      00755
  retries   5
end

execute "Unzip razor torquebox" do
  command "unzip -q -u -o #{Chef::Config[:file_cache_path]}/razor-torquebox.zip -d #{Chef::Config[:file_cache_path]}/razor-torquebox-unzipped"
  cwd "#{Chef::Config[:file_cache_path]}"
end

execute "Copy unzipped directory to torquebox installation dir" do
  command "rsync -a #{Chef::Config[:file_cache_path]}/razor-torquebox-unzipped/*/ #{node['razor']['torquebox']['dest']}"
  cwd "#{Chef::Config[:file_cache_path]}"
end

file "#{node['razor']['torquebox']['dest']}/jruby/bin/jruby" do
  mode 0755
end

directory "#{node['razor']['torquebox']['dest']}/jboss/standalone" do
  owner node['razor']['torquebox']['user']
  group node['razor']['torquebox']['group']
  recursive true
end

directory '/var/log/razor-server' do
  owner node['razor']['torquebox']['user']
  group 'root'
  mode  00755
  recursive true
end

template '/etc/init.d/razor-server' do
  source 'razor-server.init.erb'
  owner 'root'
  group 'root'
  mode  00755
  variables(
    dest: node['razor']['torquebox']['dest'],
    user: node['razor']['torquebox']['user']
  )
end

service 'razor-server' do
  action [:enable, :start]
end